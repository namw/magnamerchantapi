from menu import Menu, MenuItem
from django.core.urlresolvers import reverse

# Add two items to our main menu
Menu.add_item("main", MenuItem("Tools",
                               reverse("upload.views.options"),
                               weight=10,
                               icon="tools"))

Menu.add_item("main", MenuItem("Reports",
                               reverse("upload.views.options"),
                               weight=20,
                               icon="report"))
