from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.options, name='options'),
    url(r'^reward_point_calculation/$', views.rewardPointsCalculation, name='rewardPointsCalculation'),
]
