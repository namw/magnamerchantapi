# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render

from .forms import rewardPointsCalculationForm

from .soap_api import soap_api

# Create your views here.
def options(request):
    return render(request, 'upload/options.html', {})

def rewardPointsCalculation(request):
    if request.method == "POST":
        form = rewardPointsCalculationForm(request.POST)
        if form.is_valid():
            post = form.save(commit=False)
            post.save()
            data =  form.cleaned_data
            print data['username']
            soap = soap_api()
            soap.send(data)
            #if request.username == "a":
            #    return render(request, 'upload/options.html', {'form': form})
        return render(request, 'upload/reward_points_calculation.html', {'form': form})

    else:
        form = rewardPointsCalculationForm()
        return render(request, 'upload/reward_points_calculation.html', {'form': form})
