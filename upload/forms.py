from django import forms

from .models import rewardPointsCalculation

class rewardPointsCalculationForm(forms.ModelForm):

    class Meta:
        model = rewardPointsCalculation
        fields = ('username', 'password','transactionId','loyaltyId','country','pointCategory','description','volume','rewardPoints','statusPoints')
