# Scotia Data Parser
import requests
import xml.etree.ElementTree as SOAP
import datetime
import re

class soap_api:
    # build xml tree structure
    root = SOAP.Element("soapenv:Envelope")
    root.set("xmlns:lms","http://lms.sixdee.com")
    root.set("xmlns:soapenv","http://schemas.xmlsoap.org/soap/envelope/")
    head = SOAP.SubElement(root, "soapenv:Header")
    body = SOAP.SubElement(root, "soapenv:Body")
    rewardPointCalculation = SOAP.SubElement(body, "lms:rewardPointCalculation")
    rewardPointCalculationRequest = SOAP.SubElement(rewardPointCalculation, "RewardPointCalculationRequest")
    validationCredentials = SOAP.SubElement(rewardPointCalculationRequest,"validationCredentials")
    username = SOAP.SubElement(validationCredentials, "userName")
    password = SOAP.SubElement(validationCredentials, "password")
    transactionId = SOAP.SubElement(rewardPointCalculationRequest,"transactionId")
    timestamp = SOAP.SubElement(rewardPointCalculationRequest,"timestamp")
    channel = SOAP.SubElement(rewardPointCalculationRequest,"channel")
    channel.text="1"
    loyaltyId = SOAP.SubElement(rewardPointCalculationRequest,"loyaltyId")
    rewardPointDetailsList = SOAP.SubElement(rewardPointCalculationRequest,"rewardPointDetailsList")
    rewardPointDetails = SOAP.SubElement(rewardPointDetailsList,"rewardPointDetails")
    pointCategory = SOAP.SubElement(rewardPointDetails,"pointCategory")
    description = SOAP.SubElement(rewardPointDetails,"description")
    volume = SOAP.SubElement(rewardPointDetails,"volume")
    pointCategory.text="1"
    description.text="ElectronicsPurchase"

    def soap_api(self):
        pass

    def send(self,data):
        headers = {'Content-Type': 'application/xml'} # set what your server accepts
        #volume.text = rows[i]["transaction-value"];
        self.volume.text = data['volume']
        self.username.text = data['username']
        self.password.text = data['password']
        ts = str(datetime.datetime.now()).split('.')[0]
        self.timestamp.text = s = re.sub('[^0-9a-zA-Z]+', '', ts)
        self.transactionId.text= data['transactionId']
        self.loyaltyId.text = data['loyaltyId']
        self.pointCategory = data['pointCategory']
        self.description = data['description']

        post_data = SOAP.tostring(self.root)
        print requests.post('http://magnaapp1.magnarewards.com:4444//LMSV2/services/PointManagement', data=post_data, headers=headers).text
