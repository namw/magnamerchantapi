# -*- coding: utf-8 -*-
from __future__ import unicode_literals

# Create your models here.
from django.db import models
from django.utils import timezone

class rewardPointsCalculation(models.Model):
    username = models.CharField(max_length=200)
    password = models.CharField(max_length=200)
    transactionId = models.CharField(max_length=200)
    loyaltyId = models.CharField(max_length=200)
    country = models.CharField(max_length=200)
    pointCategory = models.CharField(max_length=200)
    description = models.CharField(max_length=200)
    volume = models.CharField(max_length=200)
    rewardPoints = models.CharField(max_length=200)
    statusPoints = models.CharField(max_length=200)

    def publish(self):
        self.published_date = timezone.now()
        self.save()

    def __str__(self):
        return "Reward Point Calculation"

class Post(models.Model):
    author = models.ForeignKey('auth.User')
    title = models.CharField(max_length=200)
    text = models.TextField()
    created_date = models.DateTimeField(
            default=timezone.now)
    published_date = models.DateTimeField(
            blank=True, null=True)

    def publish(self):
        self.published_date = timezone.now()
        self.save()

    def __str__(self):
        return self.title
